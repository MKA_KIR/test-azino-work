import React, {createRef, useState, useEffect} from 'react';
import {createUpdateBalance} from '../../store/actions'
import {useDispatch} from 'react-redux';
import {triggerSlotRotation, debuggingSlotRotation} from './functions'

import './SlotMachine.scss'

const slotValues = [1, 2, 3, 4, 5, 6, 7, 8, 9]
const SlotMachine = ({close}) => {
    const [result, setResult] = useState([0, 0, 0])
    const [rolling, setRolling] = useState(false)
    const slotRef = [createRef(), createRef(), createRef()]
    const dispatch = useDispatch()
    useEffect(()=>{
        if (!rolling && result[0]===0){
            if (result[0]===result[1] && result[0]===result[2]){
                const prize = result[0]===7
                const spinAction = createUpdateBalance(prize)
                dispatch(spinAction)
            }
            else if (result[0]===result[1] || result[0]===result[2] || result[0]===result[3]){
                const spinAction = createUpdateBalance(0.5)
                dispatch(spinAction)
            }
        }
    },[rolling])

    const spinSlot = () => {
        setRolling(true)
        setTimeout(() => setRolling(false), 700)
        const spinAction = createUpdateBalance(-1)
        dispatch(spinAction)
       const newResult = slotRef.map((slot, i) => {
            const newNumber = triggerSlotRotation(slot.current);
            return newNumber
        });
        setResult(newResult)
    }
    const debugSlot = () => {
        const newResult = slotRef.map((slot, i) => {
                const newNumber = debuggingSlotRotation(slot.current);
                return newNumber
            }
        );
        setResult(newResult)
    }

    const slotElements = result.map((item, idx) =>
        <div className="slot-result-item" key={idx}>
            <section className='slot-result-item-section'>
                <div className="slot-result-item-container" ref={slotRef[idx]}>
                    {slotValues.map((number, i) => (
                        <div key={i}>
                            <span>{number}</span>
                        </div>
                    ))}
                </div>
            </section>
        </div>)

    return (
        <div className="slot-machine">
            <div className="slot-result">
                {slotElements}
            </div>
            <div className="slot-machine-actions">
                <button className='slot-machine-action' onClick={spinSlot}>start</button>
                <button className='slot-machine-action' onClick={debugSlot}>debugging</button>
                <button className='slot-machine-action' onClick={close}>close</button>
            </div>
        </div>
    );
};

export default SlotMachine;
